import json
import os
import tempfile
from datetime import datetime, timedelta, date

from google.oauth2 import service_account
from oauth2client.service_account import ServiceAccountCredentials

# Date variables
yesterday_dt = datetime.today().date() - timedelta(days=1)
days_14_dt = datetime.today().date() - timedelta(days=14)
days_90_dt = datetime.today().date() - timedelta(days=94)
months_3_dt = date(days_90_dt.year, days_90_dt.month, 1)

yesterday_str = yesterday_dt.strftime("%Y-%m-%d")
days_14_str = days_14_dt.strftime("%Y-%m-%d")
days_90_str = days_90_dt.strftime("%Y-%m-%d")
months_3_str = months_3_dt.strftime("%Y-%m-%d")

# ga_functions variables
# Google Ads API Auth variables
ga_customer_id = os.environ.get("GA_CUSTOMER_ID")

# Google Ads API query variables
ga_ad_fields_list = [
    "segments.date",
    "campaign.name",
    "campaign.advertising_channel_type",
    "ad_group.name",
    "ad_group_ad.ad.image_ad.name",
    "ad_group_ad.ad.id",
    "metrics.cost_micros",
    "metrics.clicks",
    "metrics.impressions",
]

ga_conversions_fields_list = [
    "segments.date",
    "segments.conversion_action_name",
    "ad_group_ad.ad.id",
    "metrics.all_conversions",
    "metrics.all_conversions_value",
]

ga_columns_dict = {
    "segments.date": "dim_date",
    "campaign.advertising_channel_type": "dim_source",
    "campaign.name": "dim_campaign",
    "ad_group.name": "dim_ad_group",
    "ad_group_ad.ad.image_ad.name": "dim_ad",
    "metrics.cost_micros": "met_spent",
    "metrics.impressions": "met_impressions",
    "metrics.clicks": "met_clicks",
    "leads": "met_leads",
    "sign_up": "met_sign_up",
    "Sales Lead": "met_sales_lead",
    "Schedule":  "met_schedule",
    "applications": "met_applications",
    "met_add_to_cart": "met_add_to_cart",
    "purchases": "met_purchases",
    "revenue": "met_purchase_value",
    "Page view": "met_landing_page_view"
}

ga_ads_query = (
    """
    SELECT
        segments.date,
        campaign.id,
        campaign.name,
        ad_group.id,
        ad_group.name,
        campaign.advertising_channel_type,
        ad_group_ad.ad.id,
        ad_group_ad.ad.name,
        ad_group_ad.ad.image_ad.name,
        metrics.cost_micros,
        metrics.clicks,
        metrics.impressions
    FROM ad_group_ad
    WHERE segments.date BETWEEN '"""
    + months_3_str
    + """' AND '"""
    + yesterday_str
    + """'
    ORDER BY segments.date ASC
    """
)

ga_conversions_query = (
    """
    SELECT
        segments.date,
        segments.conversion_action_name,
        ad_group_ad.ad.id,
        metrics.all_conversions,
        metrics.all_conversions_value,
        metrics.view_through_conversions
    FROM ad_group_ad
    WHERE
        segments.date BETWEEN '"""
    + months_3_str
    + """' AND '"""
    + yesterday_str
    + """' 
        AND segments.conversion_action_name IN ('Submitted Application', 'Purchase', 'Generate Lead', 'Schedule', 'Sales Lead', 'Sign Up', 'Add to cart', 'Page view')
    ORDER BY segments.date ASC
    """
)

# fb_functions variables
# Facebook API Auth variables
fb_access_token = os.environ.get("FB_ACCESS_TOKEN")
fb_ad_account_id = os.environ.get("FB_ACCOUNT_ID")
fb_app_secret = os.environ.get("FB_APP_SECRET")
fb_app_id = os.environ.get("FB_APP_ID")

# Facebook reporting parameters and fields
fb_params_dict = {
    "time_range": {"since": months_3_str, "until": yesterday_str},
    "time_increment": 1,
    "level": "ad",
}

fb_fields_list = [
    "campaign_name",
    "adset_name",
    "ad_name",
    "spend",
    "impressions",
    "reach",
    "clicks",
    "actions",
    "action_values",
    "conversions",
    "conversion_values"
]

fb_actions_list = [
    "landing_page_view",
    "omni_view_content",
    "lead",
    "complete_registration",
    "offsite_conversion.fb_pixel_custom.SalesLead",
    "add_to_cart",
    "initiate_checkout",
    "schedule_total",
    "omni_purchase",
    "offsite_conversion.fb_pixel_custom",
    "offsite_conversion.custom.3234878993405176",
    "offsite_conversion.custom.738056647401802", # Retreat purchase
    "offsite_conversion.custom.588250142685261", # Trip purchase
    "offsite_conversion.custom.3962095924014820" # Journey purchase
]

fb_columns_dict = {
    "date": "dim_date",
    "source": "dim_source",
    "campaign_name": "dim_campaign",
    "adset_name": "dim_ad_group",
    "ad_name": "dim_ad",
    "spend": "met_spent",
    "reach": "met_reach",
    "impressions": "met_impressions",
    "clicks": "met_clicks",
    "omni_view_content": "met_view_content",
    "landing_page_view": "met_landing_page_view",
    "lead": "met_leads",
    "complete_registration": "met_sign_up",
    "offsite_conversion.fb_pixel_custom.SalesLead": "met_sales_lead",
    "applications": "met_applications",
    "schedule_total": "met_schedule",
    "add_to_cart": "met_add_to_cart",
    "initiate_checkout": "met_initiate_checkout",
    "omni_purchase": "met_purchases",
    "offsite_conversion.custom.738056647401802": "met_retreat", # Retreat purchase
    "offsite_conversion.custom.588250142685261": "met_trip", # Trip purchase
    "offsite_conversion.custom.3962095924014820": "met_journey", # Journey purchase

    "omni_view_content_value": "met_view_content_value",
    "landing_page_view_value": "met_landing_page_view_value",
    "lead_value": "met_leads_value",
    "applications_value": "met_applications_value",
    "schedule_total_value": "met_schedule_value",
    "add_to_cart_value": "met_add_to_cart_value",
    "initiate_checkout_value": "met_initiate_checkout_value",
    "omni_purchase_value": "met_purchase_value",
}

fb_columns_list = (
    fb_fields_list[:-2] + fb_actions_list + [x + "_value" for x in fb_actions_list]
)
fb_columns_list.insert(0, "date")

# Establish BigQuery credentials
bq_account_creds = json.loads(os.environ.get("BQ_ACCOUNT_CREDS"))
bq_credentials = service_account.Credentials.from_service_account_info(bq_account_creds)

bq_project_id = "paid-digital-data-blend"
bq_table_id = (
    "paid-digital-data-blend.paid_digital_performance.paid_digital_performance_all"
)

bq_schema_list = [
    {"name": "dim_date", "type": "DATETIME", "mode": "REQUIRED"},
    {"name": "dim_source", "type": "STRING", "mode": "REQUIRED"},
    {"name": "dim_campaign", "type": "STRING", "mode": "REQUIRED"},
    {"name": "dim_ad_group", "type": "STRING", "mode": "REQUIRED"},
    {"name": "dim_ad", "type": "STRING", "mode": "REQUIRED"},
    {"name": "dim_objective", "type": "STRING", "mode": "NULLABLE"},
    {"name": "dim_productline", "type": "STRING", "mode": "NULLABLE"},
    {"name": "dim_product", "type": "STRING", "mode": "NULLABLE"},
    {"name": "dim_program", "type": "STRING", "mode": "NULLABLE"},
    {"name": "met_spent", "type": "FLOAT64", "mode": "REQUIRED"},
    {"name": "met_reach", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_impressions", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_clicks", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_view_content", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_landing_page_view", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_leads", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_sign_up", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_sales_lead", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_applications", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_schedule", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_add_to_cart", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_initiate_checkout", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_purchases", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_retreat", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_trip", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_journey", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_view_content_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_landing_page_view_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_leads_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_schedule_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_applications_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_add_to_cart_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_initiate_checkout_value", "type": "FLOAT64", "mode": "NULLABLE"},
    {"name": "met_purchase_value", "type": "FLOAT64", "mode": "NULLABLE"},
]

# Google sheet information
tracker_url = "https://docs.google.com/spreadsheets/d/1seQLgtCn7mJgpQxEtRa8uKsft3CwoI-qJ0HiG9hrny8/edit#gid=1504545389"
tracker_gs_key = "1seQLgtCn7mJgpQxEtRa8uKsft3CwoI-qJ0HiG9hrny8"
social_weekly_wks = "Paid Social - Weekly"
social_weekly_wks_id = "39281733"
social_monthly_wks = "Paid Social - Monthly"
social_monthly_wks_id = "600837986"
creatives_gs_key = "1oL1qkGOvDI_fPfJcpRzW4ge_dQ7wdRRTEGkoa4lkXI4"
creatives_gs_wks = "Creatives"


# Establish Commercial Tracker Google Sheet credentials
def _google_creds_as_file():
    _temp = tempfile.NamedTemporaryFile(dir=".", delete=False)
    _temp.write(
        json.dumps(
            {
                "type": os.environ.get("GOOGLE_ACCOUNT_TYPE"),
                "project_id": os.environ.get("GOOGLE_PROJECT_ID"),
                "private_key_id": os.environ.get("GOOGLE_PRIVATE_KEY_ID"),
                "private_key": os.environ.get("GOOGLE_PRIVATE_KEY").replace(
                    "\\n", "\n"
                ),
                "client_email": os.environ.get("GOOGLE_CLIENT_EMAIL"),
                "client_id": os.environ.get("GOOGLE_CLIENT_ID"),
                "auth_uri": os.environ.get("GOOGLE_AUTH_URI"),
                "token_uri": os.environ.get("GOOGLE_TOKEN_URI"),
                "auth_provider_x509_cert_url": os.environ.get("GOOGLE_PROVIDER_URL"),
                "client_x509_cert_url": os.environ.get("GOOGLE_CERT_URL"),
            }
        ).encode("utf-8")
    )
    _temp.close()
    return _temp


gs_credentials = _google_creds_as_file()

# Establish RY Creative directory Google Sheet credentials
SCOPES = [
    "https://www.googleapis.com/auth/spreadsheets",
    "https://www.googleapis.com/auth/drive",
]
API_NAME = "sheets"
API_VERSION = "v4"

cd_account_creds = json.loads(os.environ.get("GS_ACCOUNT_CREDS"))
cd_credentials = ServiceAccountCredentials.from_json_keyfile_dict(cd_account_creds)

# LinkedIn variables
linkedin_dict = {
    "Start Date (in UTC)": "dim_date",
    "source": "dim_source",
    "Campaign Name": "dim_campaign",
    "DSC Name": "dim_ad_group",
    "Creative Name": "dim_ad",
    "Total Spent": "met_spent",
    "Reach": "met_reach",
    "Impressions": "met_impressions",
    "Clicks": "met_clicks",
    "Leads": "met_leads",
    "applications": "met_applications",
    "purchases": "met_purchases",
    "applications_value": "met_applications_value",
    "purchases_value": "met_purchase_value",
}

# Create a list of columns that need to be converted to floats at the end
creatives_dict = {
    "Objective": "dim_objective",
    "Productline": "dim_productline",
    "Product": "dim_product",
    "Program": "dim_program",
}

agg_dict = {
    "met_impressions": "sum",
    "met_clicks": "sum",
    "met_leads": "sum",
    "met_applications": "sum",
    "met_purchases": "sum",
    "met_purchase_value": "sum",
    "met_spent": "sum",
}

# Micsosoft Ads variables
ms_bq_query_str = '''
SELECT
    ads.date AS dim_date,
    ads.campaign_id AS dim_campaign_id,
    (SELECT MAX(campaigns.name)
        FROM `ry-data-warehouse-18648.bingads.campaign_history` campaigns
        WHERE ads.campaign_id = campaigns.id)
    AS dim_campaign,
    (SELECT MAX(ad_groups.name)
        FROM `ry-data-warehouse-18648.bingads.ad_group_history` ad_groups
        WHERE ads.ad_group_id = ad_groups.id)
    AS dim_ad_group,
    ads.ad_id AS dim_ad,
    SUM(ads.spend) AS met_spent,
    SUM(ads.impressions) AS met_impressions,
    SUM(ads.clicks) AS met_clicks

FROM `ry-data-warehouse-18648.bingads.ad_performance_daily_report` AS ads
WHERE ads.date BETWEEN DATE_SUB(current_date(), INTERVAL 93 DAY) AND DATE_SUB(current_Date(), INTERVAL 1 DAY)
GROUP BY dim_date, dim_campaign_id, dim_campaign, dim_ad_group, dim_ad
ORDER BY dim_date ASC
'''

ga4_bq_query_str = '''
SELECT
    event_date AS dim_date,
    event_timestamp AS dim_event_time,
    user_pseudo_id AS dim_user_id,
    event_name AS dim_event_name,
    COALESCE(CAST((SELECT value.int_value FROM UNNEST(event_params) WHERE key='campaign') AS STRING), traffic_source.name) AS dim_campaign,
    COALESCE((SELECT value.string_value FROM UNNEST(event_params) WHERE key='source'), traffic_source.source) AS dim_source,
    (SELECT value.int_value FROM UNNEST(event_params) WHERE key='content') AS dim_ad,
    COALESCE((SELECT value.string_value FROM UNNEST(event_params) WHERE key='term'), traffic_source.medium) AS dim_ad_group,
    (CASE
      WHEN event_name = 'purchase' THEN (SELECT value.int_value FROM UNNEST(event_params) WHERE key='value')
      ELSE 0
    END) AS met_purchase_value
FROM `ry-data-warehouse-18648.analytics_259990037.events_*`, 
UNNEST(event_params) AS params
WHERE 
    (_TABLE_SUFFIX BETWEEN FORMAT_DATE('%Y%m%d', DATE_SUB(CURRENT_DATE(), INTERVAL 93 DAY)) AND FORMAT_DATE('%Y%m%d', DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)))
    AND ((SELECT value.string_value FROM UNNEST(event_params) WHERE key='source') = 'bing')
    AND ((event_name = 'purchase')
    OR (event_name = 'sales_lead')
    OR (event_name = 'application_submit')
    OR (event_name = 'generate_lead')
    OR (event_name = 'begin_scheudle')
    OR (event_name = 'schedule')
    OR (event_name = 'add_to_cart')
    OR (event_name = 'sign_up')
    OR (event_name = 'user_engagement'))
ORDER BY dim_date ASC
'''
