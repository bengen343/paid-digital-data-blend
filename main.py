import importlib
import os

from flask import Flask

app = Flask(__name__)

@app.route('/')
def main():
    import paid_digital_data_blend
    importlib.reload(paid_digital_data_blend)
    return ("App run completed successfully")

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))