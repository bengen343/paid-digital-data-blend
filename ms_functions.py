import numpy as np
import pandas as pd

from config import *

def ga4_to_df():
    ga4_bq_df = pd.read_gbq(ga4_bq_query_str, project_id='ry-data-warehouse-18648', credentials=bq_credentials)
    ga4_bq_df = ga4_bq_df.drop_duplicates(['dim_event_time', 'dim_user_id'])
    ga4_bq_df = ga4_bq_df.fillna(0)
    ga4_bq_df['dim_date'] = pd.to_datetime(ga4_bq_df['dim_date']).dt.date.astype('str')
    ga4_bq_df['dim_campaign'] = ga4_bq_df['dim_campaign'].astype('str')

    return ga4_bq_df


# Microsoft Ads data from the RY Data Warehouse
def ms_to_df():
    conversion_dict = {
        'generate_lead': 'met_leads',
        'sales_lead': 'met_sales_lead',
        'application_submit': 'met_applications',
        'add_to_cart': 'met_add_to_cart',
        'sign_up': 'met_sign_up',
        'schedule': 'met_schedule',
        'purchase': 'met_purchases',
        'user_engagement': 'met_landing_page_view'
    }

    microsoft_bq_df = pd.read_gbq(ms_bq_query_str, project_id='ry-data-warehouse-18648', credentials=bq_credentials)
    microsoft_bq_df['dim_source'] = 'bing'
    microsoft_bq_df = microsoft_bq_df.fillna(0)
    microsoft_bq_df['dim_date'] = pd.to_datetime(microsoft_bq_df['dim_date']).dt.date.astype('str')
    microsoft_bq_df['dim_campaign_id'] = microsoft_bq_df['dim_campaign_id'].astype('int64').astype('str')
    microsoft_bq_df['met_leads'] = 0
    microsoft_bq_df['met_sales_lead'] = 0
    microsoft_bq_df['met_sign_up'] = 0
    microsoft_bq_df['met_applications'] = 0
    microsoft_bq_df['met_add_to_cart'] = 0
    microsoft_bq_df['met_schedule'] = 0
    microsoft_bq_df['met_purchases'] = 0
    microsoft_bq_df['met_purchase_value'] = 0
    microsoft_bq_df['met_landing_page_view'] = 0

    ga4_bq_df = ga4_to_df()

    ga4_bq_df = ga4_bq_df.drop_duplicates(['dim_date', 'dim_user_id', 'dim_event_name'])

    counts_df = ga4_bq_df.groupby(['dim_date', 'dim_campaign', 'dim_event_name']).agg({'dim_user_id': 'count', 'met_purchase_value': 'sum'})

    for i in counts_df.index:
        date_str = i[0]
        campaign_str = i[1]
        conversion_str = i[2]
        value_str = counts_df.loc[i, 'dim_user_id']

        index_int = microsoft_bq_df[(microsoft_bq_df['dim_date'] == date_str) & (microsoft_bq_df['dim_campaign_id'] == campaign_str)].index.min()
        microsoft_bq_df.loc[index_int, conversion_dict.get(conversion_str)] = value_str

        if conversion_str == 'purchase':
            value_str = counts_df.loc[i, 'met_purchase_value']
            microsoft_bq_df.loc[index_int, 'met_purchase_value'] = value_str

    microsoft_bq_df = microsoft_bq_df.drop('dim_campaign_id', 1)
    microsoft_bq_df = microsoft_bq_df.drop(np.nan, 0)

    return microsoft_bq_df
