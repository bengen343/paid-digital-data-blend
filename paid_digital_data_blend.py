#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import importlib
import pygsheets
from df2gspread import gspread2df as g2d
from google.cloud import bigquery
from googleapiclient.discovery import build

import config
from config import *

importlib.reload(config)

from fb_functions import fb_to_df
from ga_functions import ga_to_df
from ms_functions import ms_to_df


# Define a function to find the Monday of the week a date falls in
def week_start(_date):
    if _date is None:
        _date = 0
    else:
        _date = pd.to_datetime(_date, errors='coerce').date()
        _date = (_date - timedelta(days=_date.weekday()))

    return _date


# Define a function to return the date of the start of the month a date falls in
def month_start(_date):
    if _date is None:
        _date = 0
    else:
        _date = pd.to_datetime(_date, errors='coerce').date()
        _date = date(_date.year, _date.month, 1)

    return _date


# Import Facebook report to dataframe
facebook_df = fb_to_df(app_id=fb_app_id,
                       app_secret=fb_app_secret,
                       access_token=fb_access_token,
                       ad_account_id=fb_ad_account_id,
                       fields_list=fb_fields_list,
                       params_dict=fb_params_dict,
                       actions_list=fb_actions_list,
                       columns_dict=fb_columns_dict)

# Import Google report to dataframe
google_df = ga_to_df(ads_query=ga_ads_query, conversions_query=ga_conversions_query, columns_dict=ga_columns_dict)

# Import Microsoft report to dataframe
microsoft_df = ms_to_df()

# Combine the respective frames into one
marketing_df = pd.concat((facebook_df, google_df, microsoft_df), axis=0)
marketing_df.reset_index(inplace=True, drop=True)

# Import the list of active creatives.
creatives_df = g2d.download(creatives_gs_key, wks_name=creatives_gs_wks, col_names=True, row_names=False,
                            credentials=cd_credentials, start_cell='A1')

# Add in descriptions and products
marketing_df['dim_ad'] = marketing_df['dim_ad'].astype('str').apply(lambda x: x.upper())
creatives_df['Asset'] = creatives_df['Asset'].astype('str').apply(lambda x: x.upper())
creatives_df = creatives_df.drop_duplicates(['Asset'])

marketing_df = pd.merge(marketing_df, creatives_df[['Asset', 'Objective', 'Productline', 'Product', 'Program']],
                        how='left', left_on='dim_ad', right_on='Asset')

marketing_df = marketing_df.rename(columns=creatives_dict)
for column in creatives_dict.values():
    marketing_df[column] = marketing_df[column].astype('str').apply(lambda x: x.lower())

del marketing_df['Asset']

# Set the formatting for the columns to make sure it's what we want
for column in [x for x in list(marketing_df) if 'dim_' in x]:
    marketing_df[column] = marketing_df[column].astype('str')

for column in [x for x in list(marketing_df) if 'met_' in x]:
    marketing_df[column] = marketing_df[column].astype('float64')

marketing_df = marketing_df.fillna(0)

# Reorder the columns to group them by metrics and dimensions
marketing_df = marketing_df[[x for x in list(marketing_df) if 'dim_' in x] + [x for x in list(marketing_df) if 'met_' in x]]

# Convert date to an actual date so we can filter by it
marketing_df['dim_date'] = pd.to_datetime(marketing_df['dim_date'], errors='coerce')

# Construct a BigQuery client object.
bq_client = bigquery.Client(credentials=bq_credentials, project=bq_credentials.project_id, )

bq_query = """
    DELETE FROM `paid-digital-data-blend.paid_digital_performance.paid_digital_performance_all`
    WHERE dim_source != 'tinder' AND dim_date > '""" + days_90_str + """'
"""
bq_job = bq_client.query(bq_query)  # Make an API request.
bq_job.result()

print(marketing_df.info())

marketing_df[marketing_df['dim_date'] > pd.to_datetime(days_90_dt)].to_gbq(destination_table=bq_table_id,
                                                                           project_id=bq_project_id,
                                                                           if_exists='append',
                                                                           table_schema=bq_schema_list,
                                                                           credentials=bq_credentials)


# Aggregate performance by weeks and months to RY Commercial Data Tracker 2021
marketing_df['dim_week_start'] = marketing_df['dim_date'].apply(lambda x: week_start(x))
marketing_df['dim_month_start'] = marketing_df['dim_date'].apply(lambda x: month_start(x))

marketing_df['dim_month_start'] = pd.to_datetime(marketing_df['dim_month_start'], errors='coerce')
marketing_df['dim_week_start'] = pd.to_datetime(marketing_df['dim_week_start'], errors='coerce')

marketing_weekly_df = marketing_df.groupby('dim_week_start').agg(agg_dict)
marketing_monthly_df = marketing_df.groupby('dim_month_start').agg(agg_dict)

# Update the weekly sheet with the weekly aggregate
# We need two services sadly, the original Google Sheets and the Python wrapped service
gs_service = build(API_NAME, API_VERSION, credentials=bq_credentials)
gsheets_client = pygsheets.authorize(service_account_file=gs_credentials.name)

# Here we're opening the weekly paid digital sheet and seeing how current it is
gs_book = gsheets_client.open_by_key(tracker_gs_key)
wks = gs_book.worksheet_by_title(social_weekly_wks)

wks_date_range = wks.range('1:1', returnas='matrix')
sum_column_indx = wks_date_range[0].index('Total')

wks_max_date = pd.to_datetime(wks_date_range[0][sum_column_indx - 2])
wks_date_range = pd.to_datetime(wks_date_range[0][1:sum_column_indx - 1])

# Define the request to google sheets that copies the last column of data into the new week.
# This spares us having to define all the formulas that live here.
request_body = {
    'requests': [
        {
            'copyPaste': {
                'source': {
                    'sheetId': social_weekly_wks_id,
                    'startColumnIndex': sum_column_indx - 2,
                    'endColumnIndex': sum_column_indx - 1
                },
                'destination': {
                    'sheetId': social_weekly_wks_id,
                    'startColumnIndex': sum_column_indx - 1,
                    'endColumnIndex': sum_column_indx
                },
                'pasteType': 'PASTE_NORMAL'
            }
        }
    ]
}

# If we've entered a new week this will add a new column into the Google sheet tracker and copy the formulas to it.
if wks_max_date < marketing_weekly_df.index.max():
    wks.insert_cols(sum_column_indx - 1, number=1, values=None, inherit=True)

    response = gs_service.spreadsheets().batchUpdate(
        spreadsheetId=tracker_gs_key,
        body=request_body
    ).execute()

wks_date_range = wks.range('1:1', returnas='matrix')
sum_column_indx = wks_date_range[0].index('Total')
start_column_int = sum_column_indx - 13

wks.update_values((2, start_column_int), [marketing_weekly_df['met_spent'][-13:].to_list()], majordim='ROWS')
wks.update_values((4, start_column_int), [marketing_weekly_df['met_impressions'][-13:].to_list()], majordim='ROWS')
wks.update_values((7, start_column_int), [marketing_weekly_df['met_clicks'][-13:].to_list()], majordim='ROWS')
wks.update_values((14, start_column_int), [marketing_weekly_df['met_applications'][-13:].to_list()], majordim='ROWS')
wks.update_values((18, start_column_int), [marketing_weekly_df['met_purchases'][-13:].to_list()], majordim='ROWS')
wks.update_values((22, start_column_int), [marketing_weekly_df['met_purchase_value'][-13:].to_list()], majordim='ROWS')

# Here we're opening the monthly digital performance sheet and seeing how current it is
gs_book = gsheets_client.open_by_key(tracker_gs_key)
wks = gs_book.worksheet_by_title(social_monthly_wks)

wks_date_range = wks.range('1:1', returnas='matrix')
sum_column_indx = wks_date_range[0].index('Total')

wks_max_date = pd.to_datetime(wks_date_range[0][sum_column_indx - 2])
wks_date_range = pd.to_datetime(wks_date_range[0][1:sum_column_indx - 1])

# Define the request to google sheets that copies the last column of data into the new month.
# This spares us having to define all the formulas that live here.
request_body = {
    'requests': [
        {
            'copyPaste': {
                'source': {
                    'sheetId': social_monthly_wks_id,
                    'startColumnIndex': sum_column_indx - 2,
                    'endColumnIndex': sum_column_indx - 1
                },
                'destination': {
                    'sheetId': social_monthly_wks_id,
                    'startColumnIndex': sum_column_indx - 1,
                    'endColumnIndex': sum_column_indx
                },
                'pasteType': 'PASTE_NORMAL'
            }
        }
    ]
}

# If we've entered a new month this will add a new column into the Google sheet tracker and copy the formulas to it.
if wks_max_date < marketing_monthly_df.index.max():
    wks.insert_cols(sum_column_indx - 1, number=1, values=None, inherit=True)

    response = gs_service.spreadsheets().batchUpdate(
        spreadsheetId=tracker_gs_key,
        body=request_body
    ).execute()

wks_date_range = wks.range('1:1', returnas='matrix')
sum_column_indx = wks_date_range[0].index('Total')
start_column_int = sum_column_indx - 4

wks.update_values((2, start_column_int), [marketing_monthly_df['met_spent'][-4:].to_list()], majordim='ROWS')
wks.update_values((4, start_column_int), [marketing_monthly_df['met_impressions'][-4:].to_list()], majordim='ROWS')
wks.update_values((7, start_column_int), [marketing_monthly_df['met_clicks'][-4:].to_list()], majordim='ROWS')
wks.update_values((14, start_column_int), [marketing_monthly_df['met_applications'][-4:].to_list()], majordim='ROWS')
wks.update_values((18, start_column_int), [marketing_monthly_df['met_purchases'][-4:].to_list()], majordim='ROWS')
wks.update_values((22, start_column_int), [marketing_monthly_df['met_purchase_value'][-4:].to_list()], majordim='ROWS')

os.remove(gs_credentials.name)